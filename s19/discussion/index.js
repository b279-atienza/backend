// console.log("Hello World!")

// [SECTION] if, else, else if Statement
// Conditional Statemnets
	// It allow us to control the flow of our program. It allows us to run a statement or instructuin if a condition is met.

let numA = -1;

// if Statement
// Executes a statement if a specified condition is "true"

/*

syntax:

if(condition){
	// codeblock or statement


}*/

if(numA < 0){
	console.log("Hello");
}

// Basic Checking
console.log(numA < 0);

// Let's update numA and run our system
numA = 0;

if(numA < 0){
	console.log("Hello again if numA is 0");
}

//  Another Example
let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York!");
}


//  else if Clause
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numH = 1;

if(numA < 0){
	console.log("Hello");
} else if (numH > 0){
	console.log("World");
}

numA = 1;
if(numA > 0){
	console.log("Hello");
} else if (numH > 0){
	console.log("World");
}

// else Statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

if(numA < 0){
	console.log("Hello")
} else if(numH === 0){
	console.log("World")
} else {
	console.log("Again")
}

/*
We cannot use else statement alone and so with else if

else{
	console.log("Hello")
}*/

/*else if (numH === 0){
	console.log("Worl");
} else{
	console.log("Again");

	We cannot use else statement alone and so with else if
	if stament should be present on our structure
}*/


/*

	STRUCTURE
	
	if

	if
	else if

	if
	else

	if
	else if
	else if
	else

	Avoid using this structure -> error


	if
	else
	else if

*/

// if, else if, else Statemnt with Functions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet.";
	} else if(windSpeed <= 61){
		return "Tropical Depression detected."
	} else if(windSpeed >= 62 && windSpeed <=88){
		return "Tropical Strom detected."
	} else if(windSpeed >= 89 && windSpeed <=117){
		return "Severe Tropical Strom detected."	
	} else {
		return "Typhoon Detected"
	}
}

message = determineTyphoonIntensity(75);
	console.log(message);

	if (message == "Tropical Strom detected."){
		console.warn(message);
	}

// Truthy and Falsy


// Truthy
	if(true){
		console.log("truthy");
	}

	if(1){
		console.log("truthy");
	}

	if([]){
		console.log("truthy");
	}

// Falsy
	if(false){
		console.log("Falsy");
	}

	if(0){
		console.log("Falsy");
	}

	if(undefined){
		console.log("Falsy");
	}

	// [SECTION] Conditional (Ternary) Oeprator
	/* 
    - The Conditional (Ternary) Operator takes in three operands:
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
	- Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
    - Syntax
        (expression) ? ifTrue : ifFalse;
*/

// Single statement execution
let ternaryResult = (1 > 18) ? true: false
console.log("Result of ternary operator: " + ternaryResult);

// Multiple Statement Execution

let name;

function isOfLegalAge(){
	name = "John";
	return "Your are of the legal age limit."
}

function isOfUnderAge(){
	name = "Jane";
	return "Your are under the age limit.";
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let LegalAge = (age >= 18) ? isOfLegalAge() : isOfUnderAge();
console.log("Result of Ternary Operator in functions: " + LegalAge + " , " + name)

// [SECTION] Switch Statement
/*
SYNTAX

switct(expression){
	case value:
		statement;
		break;
	default:
		statement;
		break;
}
*/

let day = prompt("What day of the week is it today").toLowerCase();
console.log(day);

switch (day){
	case "monday":
		console.log("The color of the day is red.");
		break; 
	case "tuesday":
		console.log("The color of the day is orange.");
		break; 
	case "wednesday":
		console.log("The color of the day is yellow.");
		break; 
	case "thursday":
		console.log("The color of the day is green.");
		break; 
	case "friday":
		console.log("The color of the day is blue.");
		break; 
	case "saturday":
		console.log("The color of the day is indigo.");
		break; 
	case "sunday":
		console.log("The color of the day is violet.");
		break; 
	default:
		console.log("Please input a valid day")

}

// [section] Try-cath-Finally

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch (error){
	console.log(typeof error);
	console.warn(error.message);
	}finally {
		alert("Intensity updates will show new alert.")
	}
}

showIntensityAlert(56)