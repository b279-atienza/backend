const User = require("../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");
const routes = require("../routes/user");

// Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0) {
            return true;
        } else {
            return false
        }
    })
}

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        //10 is the value provided as the number opf salt rounds.
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if (error){
            return false;
        } else {
            return true;
        }
    })
};

// function to login a user.
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        console.log(result)
        if(result == null){
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            } else{
                return false
            }
        }
    })
}

// Create a controller function for retrieving a specific task.
module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.userId).then(result => {
        if(result == null) {
            return false;
        } else {
            result.password = "";
            return result;
        }
    });

};

// Enroll User  to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

module.exports.enroll = async (data) => {

	if(data.isAdmin){
		return "YOU ARE ADMIN, ONLY USER CAN DO THIS ACTION";
	}else{

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
		if(isUserUpdated && isCourseUpdated){
			return "You are now enrolled!";
		}else{
			return "Please try again later!";
		}

	}
	
}

