const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId : {
		type: String,
		required: [true, "userId is required"]
	},
	products : [
					{
					product: {
						type: String,
						required: [true, "Product ID is required"]				
					},

					name : {
						type : String,
						required : [true, "Product name is required!"],
					},

					description : {
						type : String,
						required : [true, "Product description is required!"]
					},
					
					price : {
						type : Number,
						required : [true, "Product price is required!"]
					},

					quantity: {
						type: Number,
                        required: [true, "Quantity is required"]
					},

					}],
	totalAmount : {
		type: Number,
		default: 0,
	},

	purchasedOn : {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);