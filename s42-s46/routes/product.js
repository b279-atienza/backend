const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");
const User = require("../models/Product")

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Get all Product
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Get all Available Products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieve specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.courseId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});

// Update a specific product admin only
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});

// Route to archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route to activate a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route to change quantity product
router.put("/:productId/quantity", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.changeQuantityFields(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route to delete product
router.delete("/:productId/delete", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.deleteProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Retrieve specific category
router.get("/category/:category", (req, res) => {
	console.log(req.params.category);
	productController.getCategory(req.params.category).then(resultFromController => res.send(resultFromController));
});

// Retrieve specific product
router.get('/trending/true', (req, res) => {
	console.log(req.query); // check if the trending parameter is in the query object
	const trending = req.query.trending;
	const trendingBool = trending === 'true'; // Convert the string value to a boolean
	productController.getTrending(trendingBool).then(result => {
	  res.send(result);
	}).catch(error => {
	  res.send(error);
	});
  });

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;