const express = require("express")
const router = express.Router()
const auth = require("../auth")
const User = require("../models/User")
const orderController = require("../controllers/orderController")
const userController = require("../controllers/userController")


//Route for registering a user.
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for User Login
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for making User to Admin
router.put("/:userId/admin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.adminUser(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route for making User to Verified
router.put("/:userId/verify", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.verifyUser(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route for adding User Balance
router.put("/:userId/topup", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.topupUser(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Get all Users
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllUsers(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route to delete user
router.delete("/:userId/delete", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.deleteUser(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route for change password
router.post("/changepassword", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getchangePassword({ userId: userData.id }, req.body)
        .then(resultFromController => res.send(resultFromController));
});

// Update a specific user
router.put("/:userId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.updateUser(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});

// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;