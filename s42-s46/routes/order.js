const express = require("express")
const router = express.Router()
const auth = require("../auth")
const orderController = require("../controllers/orderController")

// Save an order
router.post("/save", auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const userId = auth.decode(req.headers.authorization).id
  const items = req.body.items
  orderController.saveOrder({ userId, items, isAdmin }).then(result => res.json(result))

}) 


// Get specific user order
router.get("/myOrders", auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const userId = auth.decode(req.headers.authorization).id
  orderController.getOrdersForUser(userId, isAdmin).then(result => res.send(result))
})

// Get all users order
router.get("/", auth.verify, (req, res) => {
  const userId = req.params.userId
  orderController.getOrdersForAllUser(userId).then(result => res.send(result))
})



// delete  users order
router.delete("/:orderId/delete", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	orderController.deleteOrder(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Get specific order id
router.get("/:orderId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	orderController.getOrdersById(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});




module.exports = router
