const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const orderController = require("./orderController")
const mongoose = require('mongoose');

// User registration
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		username : reqBody.username,
		email : reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if (error) {
			return false;
		// User registration successful
		} else {
			return true
			// true
		};
	});
};

// User authentication.
module.exports.loginUser = (reqBody) => {
	return User.findOne({username : reqBody.username}).then(result => {
		// User does not exist
		if(result == null){
			return false
			// false
		// User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {
				// Generate an access token
				return { access : auth.createAccessToken(result) }
			// Passwords do not match
			} else {
				return false
				// false
			};
		};
	});
};

// Retrieve user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	});
};

// Set User to Admin
module.exports.adminUser = (reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided userId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.userId)) {
		return Promise.resolve(`No user ID found for ID: ${reqParams.userId}`);
	  }

	let updateActiveField = {
		isAdmin : true
	};

	if(isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((user, error) => {

			// User not admin
			if (error) {
				return false
				// false
			// Set user to admin successfully
			} else {
				return false
				//true
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have sufficient permissions to set user to admin");

	return message.then((value) => {
		return value
	})
};

// Set User to Verified User
module.exports.verifyUser = (reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided userId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.userId)) {
		return Promise.resolve(`No user ID found for ID: ${reqParams.userId}`);
		}
		
	let updateVerificationField = {
		verified : true
	};

	if(isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, updateVerificationField).then((user, error) => {

			// User not verified
			if (error) {
				return false
				// false
			// Set user to verified successfully
			} else {
				return true
				//true
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have sufficient permissions to set user verification");

	return message.then((value) => {
		return value
	})
};

// Top-up User
module.exports.topupUser = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);
  
	// Check if the provided userId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.userId)) {
	  return Promise.resolve(`No user ID found for ID: ${reqParams.userId}`);
	}
  
	let updateBalanceField = {
	  balance: reqBody.balance,
	};
  
	if (isAdmin) {
	  return User.findByIdAndUpdate(reqParams.userId, { $inc: { balance: reqBody.balance }, }).then((user, error) => {
		// User not verified
		if (error) {
		  return false
		} else {
		  return true
		}
	  });
	}
  
	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have sufficient permissions to top up user balance.");
  
	return message.then((value) => {
	  return value;
	});
  };

// retrieve all users 
module.exports.getAllUsers = (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		return User.find(reqParams.userId).then(user => {
			user.password = "";
			// Returns the user information with the password as an empty string
			return user;
		});
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have sufficient permissions to retrieve all users");

	return message.then((value) => {
		return value
	})
};

// Delete specific user
module.exports.deleteUser = ( reqParams, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided userId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.userId)) {
		return Promise.resolve(`No user ID found for ID: ${reqParams.userId}`);
		}

	if(isAdmin){
		return User.findByIdAndRemove(reqParams.userId).then((removedUser) => {
			if (!removedUser) {
			  return false
			}
			return true
		  })
	  };

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have sufficient permissions to delete user");

	return message.then((value) => {
		return value
	})
};
// Retrieve user details
module.exports.getchangePassword = (data, reqBody) => {
	return User.findById(data.userId).then(user => {
	  if (!user) {
		throw new Error('User not found');
	  }
  
	  // Check if current password is correct
	  if (!user.validPassword(reqBody.currentPassword)) {
		throw new Error('Current password is incorrect');
	  }
  
	  // Hash the new password
	  const hashedPassword = bcrypt.hashSync(reqBody.newPassword, 10);
  
	  // Update password
	  user.password = hashedPassword;
	  return user.save().then(result => {
		result.password = '';
		return result;
	  });
	});
  };


  // Update a user
module.exports.updateUser = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	// Check if the provided userId is valid
	if (!mongoose.Types.ObjectId.isValid(reqParams.userId)) {
		return Promise.resolve(`No user ID found for ID: ${reqParams.userId}`);
	}

	if(isAdmin){
		let updatedUser = {
			username : reqBody.username,
			email : reqBody.email,
			isAdmin : reqBody.isAdmin,
			verified : reqBody.verified,
			balance : reqBody.balance
		}
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			}else{
				return "User Updated!";
			}
		})
	}	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})	
}