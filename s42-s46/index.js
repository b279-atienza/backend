// Server Creation and DB Connection
const express = require("express");
const  mongoose = require("mongoose");
const cors = require ("cors");

// Allows us to control the App's Cross Origin Resource Sharing
const  userRoutes = require("./routes/user.js");
const  productRoutes = require("./routes/product.js")
const  orderRoutes = require("./routes/order.js")

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.70nxwme.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Validation of DB Connection
mongoose.connection.once("open", () => console.log("Connected to MongoDB Atlas."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 5000, () => console.log(`API is now online on port ${process.env.PORT || 5000}`));
}

module.exports = app;