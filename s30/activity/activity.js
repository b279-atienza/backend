//Result of MongoDB Aggregation to count the total number of fruits on sale.
db.fruits.aggregate([
  {$match: { onSale: true }},
  {$group: { _id: null, totalFruitsOnSale: { $sum: 1 } }},
  {$project: {_id: 0}}
]);


// Result of MongoDB Aggregation to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
  {$match: { stock: { $gte: 20 }}},
  {$group: { _id: null, enoughStock: { $sum: 1 } }},
  {$project: {_id: 0}}
]);


// Result of MongoDB Aggregation to get the average price of fruits onSale per supplier
db.fruits.aggregate([
  {$match: { onSale: true }},
  {$group: {_id: "$supplier_id",avgPrice: { $avg: "$price" }}}
]);





// Result of MongoDB Aggregation to get the highest price of a fruit onSale per supplier
db.fruits.aggregate([
  {$match: { onSale: true }},
  {$group: {_id: "$supplier_id",max_price: { $max: "$price" }}},
  {$sort: {total: -1}}
]);
// Result of MongoDB Aggregation to get the highest price of a fruit per supplier
db.fruits.aggregate([
  {$group: {_id: "$supplier_id",max_price: { $max: "$price" }}},
  {$sort: {total: -1}}
]);





// Result of MongoDB Aggregation to to get the lowest price of a fruit onSale per supplier.
db.fruits.aggregate([
  {$match: { onSale: true }},
  {$group: {_id: "$supplier_id",min_price: { $min: "$price" }}},
  {$sort: {total: -1}}
]);
// Result of MongoDB Aggregation to to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
  {$group: {_id: "$supplier_id",min_price: { $min: "$price" }}},
  {$sort: {total: -1}}
]);


